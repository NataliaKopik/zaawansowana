package Kolekcje;

import Kolekcje.MyArrayList.ElementNotFoundInMyArrayListException;
import Kolekcje.MyArrayList.MyArrayList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {
    MyArrayList<Integer> myList = new MyArrayList<>();


    @Test
    public void addShouldAddFewObjects() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        myList.add(10);
        myList.add(20);
        myList.add(30);

        assertEquals(3, myList.size());
        assertEquals(10, myList.get(0));
        assertEquals(20, myList.get(1));
        assertEquals(30, myList.get(2));
    }

    @Test
    public void addShouldHandleMoreThan10Elements() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        assertEquals(11, myList.size());
        assertEquals(10, myList.get(0));
        assertEquals(20, myList.get(1));
        assertEquals(30, myList.get(2));
        assertEquals(40, myList.get(3));
        assertEquals(50, myList.get(4));
        assertEquals(60, myList.get(5));
        assertEquals(70, myList.get(6));
        assertEquals(80, myList.get(7));
        assertEquals(90, myList.get(8));
        assertEquals(100, myList.get(9));
        assertEquals(110, myList.get(10));
    }

    @Test
    public void addShouldAllowToAddNull() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);

        assertNull(myList.get(1));
        assertEquals(3, myList.size());
    }

    @Test
    public void addWithIndexShouldAddElementWithCorrectIndex() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);
        myList.add(30, 1);

        assertEquals(4, myList.size());
        assertEquals(10, myList.get(0));
        assertEquals(30, myList.get(1));
        assertNull(myList.get(2));
        assertEquals(20, myList.get(3));
    }

    @Test
    public void addWithIndexShouldAddElementsWithCorrectIndexAndGrow() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }
        myList.add(200, 1);
        myList.add(210, 10);
        assertEquals(13, myList.size());
        assertEquals(10, myList.get(0));
        assertEquals(200, myList.get(1));
        assertEquals(20, myList.get(2));
        assertEquals(30, myList.get(3));
        assertEquals(40, myList.get(4));
        assertEquals(50, myList.get(5));
        assertEquals(60, myList.get(6));
        assertEquals(70, myList.get(7));
        assertEquals(80, myList.get(8));
        assertEquals(90, myList.get(9));
        assertEquals(210, myList.get(10));
        assertEquals(100, myList.get(11));
        assertEquals(110, myList.get(12));
    }

    @Test
    public void addWithIndexShouldThrowException() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);
        try {
            myList.add(30, 8);
        } catch (Exception e) {
            assertEquals(ArrayIndexOutOfBoundsException.class, e.getClass());
        }

        assertEquals(3, myList.size());
        assertEquals(10, myList.get(0));
        assertNull(myList.get(1));
        assertEquals(20, myList.get(2));
    }

    @Test
    public void setShouldReplaceElement() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);

        myList.set(1, 30);

        assertEquals(3, myList.size());
        assertEquals(10, myList.get(0));
        assertEquals(30, myList.get(1));
        assertEquals(20, myList.get(2));
    }

    @Test
    public void setShouldThrowException() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);

        try {
            myList.set(5, 30);
        } catch (Exception e) {
            assertEquals(ArrayIndexOutOfBoundsException.class, e.getClass());
        }

        assertEquals(3, myList.size());
        assertEquals(10, myList.get(0));
        assertNull(myList.get(1));
        assertEquals(20, myList.get(2));
    }

    @Test
    public void isEmptyShouldFalse() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }
        assertEquals(myList.isEmpty(), false);
    }

    @Test
    public void isEmptyShouldTrue() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        assertEquals(myList.isEmpty(), true);
    }

    @Test
    public void removeShouldFewElements() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }
        myList.remove(10);
        myList.remove(110);
        assertEquals(9, myList.size());
        assertEquals(20, myList.get(0));
        assertEquals(30, myList.get(1));
        assertEquals(40, myList.get(2));
        assertEquals(50, myList.get(3));
        assertEquals(60, myList.get(4));
        assertEquals(70, myList.get(5));
        assertEquals(80, myList.get(6));
        assertEquals(90, myList.get(7));
        assertEquals(100, myList.get(8));
    }

    @Test
    public void removeShouldThrowException() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        try {
            myList.remove(150);
        } catch (Exception e) {
            assertEquals(ElementNotFoundInMyArrayListException.class, e.getClass());
        }

        assertEquals(11, myList.size());
        assertEquals(10, myList.get(0));
        assertEquals(20, myList.get(1));
        assertEquals(30, myList.get(2));
        assertEquals(40, myList.get(3));
        assertEquals(50, myList.get(4));
        assertEquals(60, myList.get(5));
        assertEquals(70, myList.get(6));
        assertEquals(80, myList.get(7));
        assertEquals(90, myList.get(8));
        assertEquals(100, myList.get(9));
        assertEquals(110, myList.get(10));
    }

    @Test
    public void containsShouldTrue() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);

        assertEquals(myList.contains(20), true);
    }

    @Test
    public void containsShouldFalse() {
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(10);
        myList.add(null);
        myList.add(20);

        assertEquals(myList.contains(30), false);
    }

    @Test
    public void getIndexShouldWork() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        assertEquals(2, myList.getIndex(30));
    }

    /*
    @Test
    public void getIndexShouldNotWork(){
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        assertEquals(-1, myList.getIndex(9));
    }*/

    @Test
    public void getIndexShouldNotWork() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        try {
            myList.getIndex(9);
        } catch (Exception e) {
            assertEquals(ElementNotFoundInMyArrayListException.class, e.getClass());
        }
    }

    @Test
    public void getShouldWork() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        assertEquals(110, myList.get(10));
    }

    @Test
    public void getShoulThrowException() {
        MyArrayList<Integer> myList = new MyArrayList<>();

        for (int i = 10; i <= 110; i += 10) {
            myList.add(i);
        }

        try {
            myList.get(12);
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
    }
}