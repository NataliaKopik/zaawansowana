package Kolekcje.MyLinkedList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyLinkedListTest {

    @Test
    void addShouldAddFewElements() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(5);
        proba.add(12);
        proba.add(10);

        assertEquals(3, proba.size());
        assertEquals(5, proba.getFirst());
        assertEquals(10, proba.getLast());
        assertEquals(5, proba.get(0));
        assertEquals(12, proba.get(1));
        assertEquals(10, proba.get(2));
    }

    @Test
    void addWithIndexShouldAddElement() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.add(0, 100);

        assertEquals(4, proba.size());
        assertEquals(100, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(100, proba.get(0));
        assertEquals(10, proba.get(1));
        assertEquals(20, proba.get(2));
        assertEquals(30, proba.get(3));
    }

    @Test
    void addWithIndexShouldAddFewElement() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.add(0, 100);
        proba.add(3, 300);

        assertEquals(5, proba.size());
        assertEquals(100, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(100, proba.get(0));
        assertEquals(10, proba.get(1));
        assertEquals(20, proba.get(2));
        assertEquals(300, proba.get(3));
        assertEquals(30, proba.get(4));
    }

    @Test
    void addWithIndexShouldThrowException() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        try {
            proba.add(3, 100);
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }

        assertEquals(3, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(20, proba.get(1));
        assertEquals(30, proba.get(2));
    }

    @Test
    void setShouldReplaceLastElement() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.set(2, 100);

        assertEquals(3, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(100, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(20, proba.get(1));
        assertEquals(100, proba.get(2));
    }

    @Test
    void setShouldReplaceFirstElement() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.set(0, 100);

        assertEquals(3, proba.size());
        assertEquals(100, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(100, proba.get(0));
        assertEquals(20, proba.get(1));
        assertEquals(30, proba.get(2));
    }

    @Test
    void SetShouldReplaceElementInTheMiddleOfList() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.set(1, 100);

        assertEquals(3, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(100, proba.get(1));
        assertEquals(30, proba.get(2));
    }

    @Test
    void SetShouldThrowException() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        try {
            proba.set(4, 100);
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }

        assertEquals(3, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(20, proba.get(1));
        assertEquals(30, proba.get(2));
    }

    @Test
    void getIndexShouldGiveCorrectIndex() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);

        assertEquals(0, proba.getIndex(10));
        assertEquals(1, proba.getIndex(20));
        assertEquals(2, proba.getIndex(30));
    }

    @Test
    void getIndexShouldThrowException() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        try {
            proba.getIndex(40);
        } catch (Exception e) {
            assertEquals(ElementNotFoundInMyLinkedListException.class, e.getClass());
        }
    }

    @Test
    void getShouldThrowException() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        try {
            proba.get(4);
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
    }

    @Test
    void getNodeShouldThrowException() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        try {
            proba.getNode(4);
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
    }

    @Test
    void getNodeShouldGiveCorrectNode() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);

        assertEquals(proba.getNode(0), proba.getFirstNode());
        assertEquals(proba.getNode(1), proba.getFirstNode().getNext());
        assertEquals(proba.getNode(2), proba.getFirstNode().getNext().getNext());

    }

    @Test
    void removeShouldRemoveElementFromTheMiddleOfList() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.remove(20);

        assertEquals(2, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(30, proba.get(1));
    }

    @Test
    void removeShouldRemoveFirstElement() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.remove(10);

        assertEquals(2, proba.size());
        assertEquals(20, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(20, proba.get(0));
        assertEquals(30, proba.get(1));
    }

    @Test
    void removeShouldRemoveLastElement() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.remove(30);

        assertEquals(2, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(20, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(20, proba.get(1));
    }

    @Test
    void removeShouldThrowException() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);

        try {
            proba.remove(50);
        } catch (Exception e) {
            assertEquals(ElementNotFoundInMyLinkedListException.class, e.getClass());
        }

        assertEquals(3, proba.size());
        assertEquals(10, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(10, proba.get(0));
        assertEquals(20, proba.get(1));
        assertEquals(30, proba.get(2));
    }

    @Test
    void isEmptyShouldBeTrue() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        proba.remove(10);
        assertEquals(20, proba.getFirst());
        assertEquals(30, proba.getLast());
        proba.remove(20);
        assertEquals(30, proba.getFirst());
        assertEquals(30, proba.getLast());
        proba.remove(30);

        assertEquals(0, proba.size());

        try {
            assertNull(proba.getNode(0));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        try {
            assertNull(proba.getNode(1));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        try {
            assertNull(proba.getNode(2));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        assertTrue(proba.isEmpty());
    }

    @Test
    void isEmptyShouldBeTrueForNewList() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        assertEquals(0, proba.size());
        assertTrue(proba.isEmpty());
        try {
            assertNull(proba.getNode(0));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        try {
            assertNull(proba.getNode(1));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        try {
            assertNull(proba.getNode(2));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
    }

    @Test
    void isEmptyShouldBeFalse() {
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);
        assertEquals(3, proba.size());
        proba.remove(10);
        assertEquals(2, proba.size());
        assertEquals(20, proba.getFirst());
        assertEquals(30, proba.getLast());
        proba.remove(20);
        assertEquals(30, proba.getFirst());
        assertEquals(30, proba.getLast());
        assertEquals(1, proba.size());

        assertEquals(proba.getNode(0), proba.getFirstNode());

        try {
            assertNull(proba.getNode(1));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        try {
            assertNull(proba.getNode(2));
        } catch (Exception e) {
            assertEquals(IndexOutOfBoundsException.class, e.getClass());
        }
        assertFalse(proba.isEmpty());
    }

    @Test
    void containShouldBeCorrect(){
        MyLinkedList<Integer> proba = new MyLinkedList<>();
        proba.add(10);
        proba.add(20);
        proba.add(30);

        assertTrue(proba.contains(10));
        assertTrue(proba.contains(20));
        assertTrue(proba.contains(30));
        assertFalse(proba.contains(300));
    }

}
