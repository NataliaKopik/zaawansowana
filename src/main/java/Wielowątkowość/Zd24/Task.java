package Wielowątkowość.Zd24;

public class Task implements Runnable {

    public void run() {
        for (int i = 0; i < 1_000; i++) {
            System.out.println(i + " klasa");
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Runnable anonimowa = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1_000; i++) {
                    System.out.println(i + " anonimowa");
                }
            }
        };
        Thread jeden = new Thread(new Task());
        Thread dwa = new Thread(anonimowa);
        jeden.start();
        jeden.join(); //Metoda zapewnia, że jeden zostanie zakończony przed wykonaniem następnej instrukcji. Bez joina obie metody start wynonywałyby się jednocześnie
        dwa.start();

    }
}
