package Wielowątkowość.Zd25;

public class ConcatenationTask implements Runnable {
    Thread thread;
    private int number;
    private char character;
    private String result = "";
    private boolean run = true;

    public ConcatenationTask(int number, char character) {
        this.number = number;
        this.character = character;
        thread = new Thread(this);
    }

    public void startTask() {
        thread.start();
    }

    public void waitTillEnd() {
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void abort() {
        run = false;
    }

    public String getResult() {
        return result;
    }

    @Override
    public void run() {
        for (int i = 0; i < number; i++) {
            if (!run) {
                break;
            }
            result += character;
        }
    }
}

