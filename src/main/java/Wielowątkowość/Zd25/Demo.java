package Wielowątkowość.Zd25;

public class Demo {
    public static void main(String[] args) {
        ConcatenationTask concatenationTask = new ConcatenationTask(200_000, 'D');
        concatenationTask.startTask();
        Thread abortingTgread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    concatenationTask.abort();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        abortingTgread.run();
       concatenationTask.waitTillEnd();
        System.out.println(concatenationTask.getResult().length());
    }
}
