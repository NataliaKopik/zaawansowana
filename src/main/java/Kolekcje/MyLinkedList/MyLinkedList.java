package Kolekcje.MyLinkedList;

public class MyLinkedList<E> {
    private MyNode<E> firstNode = null;
    private int actualSize = 0;

    MyNode<E> getFirstNode() {
        return firstNode;
    }

    @Override
    public String toString() {
        return "MyLinkedList{" +
                "firstNode=" + firstNode +
                ", actualSize=" + actualSize +
                '}';
    }

    public void add(E element) {
        MyNode<E> node = new MyNode<>(element);
        if (firstNode == null) {
            firstNode = node;
        } else {
            MyNode<E> currentNode = firstNode;
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
            node.previous = currentNode;
        }
        actualSize++;
    }

    /*public void add(E element) {
        MyNode<E> node = new MyNode<>(element);
        if (firstNode == null) {
            firstNode = node;
        } else {
            MyNode<E> currentNode = firstNode;
            boolean done = false;
            while (done == false) {
                if (currentNode.next == null) {
                    currentNode.next = node;
                    node.previous = currentNode;
                    done = true;
                } else currentNode = currentNode.next;
            }
        }
        actualSize++;
    }*/

    public void add(int index, E element) {
        if (actualSize <= index) {
            throw new IndexOutOfBoundsException();
        }
        MyNode<E> node = new MyNode<>(element);
        if (index == 0) {
            node.next = firstNode;
            firstNode.previous = node;
            firstNode = node;
        } else {
            MyNode<E> oldNode = getNode(index);
            node.next = oldNode;
            node.previous = oldNode.previous;
            oldNode.previous = node;
            node.previous.next = node;
        }
        actualSize++;
    }

    public void set(int index, E element) {
        if (actualSize <= index) {
            throw new IndexOutOfBoundsException();
        }
        MyNode<E> node = new MyNode<>(element);
        if (index == 0) {
            node.next = firstNode.next;
            firstNode = node;
        } else if (actualSize == index + 1) {
            MyNode<E> oldNode = getNode(index);
            node.previous = oldNode.previous;
            oldNode.previous = null;
            node.previous.next = node;
        } else {
            MyNode<E> oldNode = getNode(index);
            node.previous = oldNode.previous;
            node.next = oldNode.next;
            oldNode.previous = null;
            oldNode.next = null;
            node.previous.next = node;
            node.next.previous = node;
        }
    }

    public void remove(E element) {
        MyNode<E> currentNode = firstNode;
        try {
            if (firstNode.element.equals(element)) {
                firstNode = firstNode.next;
                // firstNode.previous.next = null;
                if (firstNode != null) {
                    firstNode.previous = null;
                }
            } else {
                while (!currentNode.element.equals(element)) {
                    currentNode = currentNode.next;
                }
                if (!(currentNode.next == null)) {
                    currentNode.next.previous = currentNode.previous;
                }
                currentNode.previous.next = currentNode.next;
                // currentNode.previous = null;
                //  currentNode.next = null;
            }
            actualSize--;
        } catch (Exception e) {
            System.out.println("Error! Element " + element + " not found!");
        }
    }

    public int size() {
        return actualSize;
    }

    public boolean isEmpty() {
        return actualSize == 0;
    }

    public boolean contains(E element) {
        MyNode<E> currentNode = firstNode;
        for (int i = 0; i < actualSize; i++) {
            if (currentNode.element.equals(element)) {
                return true;
            }
            currentNode = currentNode.next;
        }
        return false;
    }

    public E get(int index) {
        if (actualSize <= index || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        MyNode<E> currentNode = firstNode;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }
        return currentNode.element;

    }

    public int getIndex(E element) {
        MyNode<E> currentNode = firstNode;
        int currentIndex = 0;
        while (!currentNode.element.equals(element)) {
            if (currentNode.next == null) {
                throw new ElementNotFoundInMyLinkedListException("Error! Element " + element + " not found");
            }
            currentIndex++;
            currentNode = currentNode.next;
        }
        return currentIndex;
    }

    /*public int getIndex(E element) {
        MyNode<E> currentNode = firstNode;
        try {
            int currentIndex = 0;
            while (!currentNode.element.equals(element)) {
                currentIndex++;
                currentNode = currentNode.next;
            }
            return currentIndex;
        } catch (Exception e) {
            System.out.println("Error! Element " + element + " not found!");
        }
        return -1;
    }*/

    public MyNode getNode(int index) {
        if (actualSize <= index) {
            throw new IndexOutOfBoundsException();
        }
        MyNode<E> currentNode = firstNode;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }
        return currentNode;
    }

    /*public MyNode getNode(int index) {
        MyNode<E> currentNode = firstNode;
        try {
            for (int i = 0; i < index; i++) {
                currentNode = currentNode.next;
            }
            return currentNode;
        } catch (Exception e) {
            System.out.println("Error! Index out of List!");
        }
        return null;
    }*/

    public E getFirst() {
        return firstNode.element;
    }

    public E getLast() {
        MyNode<E> currentNode = firstNode;
        while (currentNode.next != null) {
            currentNode = currentNode.next;
        }
        return currentNode.element;
    }


    class MyNode<E> {
        private E element;
        private MyNode<E> previous;

        @Override
        public String toString() {
            return "MyNode{" +
                    "element=" + element +
                    "next=" + next +
                    '}';
        }

        private MyNode<E> next = null;

        public E getElement() {
            return element;
        }

        public MyNode<E> getPrevious() {
            return previous;
        }

        public MyNode<E> getNext() {
            return next;
        }

        public MyNode(E element) {
            this.element = element;
        }

    }
}
