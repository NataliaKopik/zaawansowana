package Kolekcje.MyArrayList;

import java.util.Arrays;

public class MyArrayList<E> {
    private E[] elements = (E[]) new Object[10];
    private int actualSize = 0;

    @Override
    public String toString() {
        return "MyArrayList{" +
                "elements=" + Arrays.toString(elements) +
                ", actualSize=" + actualSize +
                '}';
    }

    public void add(E element) {
        try {
            elements[actualSize] = element;
            actualSize++;
        } catch (ArrayIndexOutOfBoundsException e) {
            grow();
            add(element);
        }
    }

    public void add(E element, int index) {
        if(index > actualSize){
            throw new ArrayIndexOutOfBoundsException();
        }
        try {
            for (int i = actualSize - 1; i >= index; i--) {
                elements[i + 1] = elements[i];
            }
            elements[index] = element;
            actualSize++;
        } catch (ArrayIndexOutOfBoundsException e) {
            grow();
            add(element, index);
        }
    }

    private void grow() {
      /*  E[] newArray = (E[]) new Object[actualSize + 5];
        for (int i = 0; i < actualSize; i++) {
            newArray[i] = elements[i];
        }
        elements = newArray;
       */
        elements = Arrays.copyOf(elements,elements.length+5);
    }

    public int size() {
        return actualSize;
    }

    public void set(int index, E element) {
        try {
            elements[index] = element;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error! Index out of ArrayList!");
        }
    }

    public boolean isEmpty() {
        return actualSize == 0;

    }

    public void remove(E element) {
        boolean done = false;
        for (int i = 0; i < actualSize; i++) {
            if (elements[i].equals(element)) {
                for (int j = i; j < actualSize; j++) {
                    elements[j] = elements[j + 1];
                }
                actualSize--;
                done = true;
                break;
            }
        }
        if (done == false) {
            throw new ElementNotFoundInMyArrayListException("Error! " + element + " not found");
            /*
            System.out.println("Error! " + element + " not found");
             */
        }
    }

    public boolean contains(E element) {
        for (E e : elements) {
            if (element.equals(e)) {
                return true;
            }
        }
        return false;
    }

    public int getIndex(E element) {
        boolean done = false;
        for (int i = 0; i < actualSize; i++){
            if (element.equals(elements[i])){
                done = true;
                return i;
            }
        }
        if (done == false){
            throw new ElementNotFoundInMyArrayListException("Error! " + element + " not found");
        }
        return -1;
    }

    /*public int getIndex(E element) {
        for (int i = 0; i < actualSize; i++){
            if (element.equals(elements[i])){
                return i;
            }
        }
        return -1;
    }*/

    public E get (int index){
        if (index < 0 || index >= actualSize) {
            throw new IndexOutOfBoundsException();
        }
        return elements[index];
    }
}
