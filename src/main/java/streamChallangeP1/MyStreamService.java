package streamChallangeP1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface MyStreamService extends StreamService {
    @Override
    default void sortAndPrint(List<String> names) {
        names.stream()
                .sorted()
                .forEach(name -> System.out.println(name));
    }

    @Override
    default int distinctAndCountNumbers(int[] numbers) {
        return Arrays.stream(numbers)
                .distinct()
                .sum();
    }

    @Override
    default List<String> computeMaleNames(List<String> names) {
        return names.stream()
                .filter(name -> name.endsWith("a"))
                //lub
                //.filter(name -> name.charAt(name.length() - 1) == 'a')
                .toList();
    }

    @Override
    default void printNumbersOfRange(int[] numbers, int minValue, int maxValue) {
        Arrays.stream(numbers)
                .filter(number -> number < maxValue && number > minValue)
                .forEach(number -> System.out.println(number));
    }

    @Override
    default List<Integer> computeNamesLength(List<String> names) {
        return names.stream()
                .map(name -> name.length())
                .toList();
    }

    @Override
    default List<Person> buildPeopleWithNames(List<String> names) {
        List<Person> people = new ArrayList<>();
        names.stream().forEach(name -> {
            MyPerson person = new MyPerson(name);
            people.add(person);
        });
        return people;
    }

    @Override
    default List<Person> findPeopleOfIdGreaterThan(List<Person> people, int id) {
        return people.stream()
                .filter(person -> person.getId() > id)
                .toList();
    }

    @Override
    default boolean hasSenior(List<Person> people) {
        return false;
    }

    @Override
    default double sumTotalCash(List<Person> people) {
        return 0;
    }

    @Override
    default Person findRichestPerson(List<Person> people) {
        return null;
    }

    @Override
    default double computeAverageAge(List<Person> people) {
        return 0;
    }
}
