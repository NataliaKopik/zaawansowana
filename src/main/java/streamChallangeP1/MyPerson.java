package streamChallangeP1;

import java.time.LocalDate;
import java.util.Random;

public class MyPerson extends Person{
    public MyPerson(String name) {
        super(name);
    }

    @Override
    protected int getRandomCash() {
        Random random = new Random();
        return random.nextInt(101);
    }

    @Override
    public LocalDate getRandomBirthDate() {
        Random random = new Random();
        return LocalDate.now().minusYears(random.nextInt(101));
    }

    @Override
    public int getAge() {
        return LocalDate.now().getYear() - getBirthDate().getYear();
    }

}
