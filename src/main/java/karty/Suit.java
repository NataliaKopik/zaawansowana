package karty;

public enum Suit {
    SPADES, HEARTS, CLUBS, DIAMONDS;
}
