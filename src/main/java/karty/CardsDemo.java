package karty;

public class CardsDemo {
    public static void main(String[] args) {
        Card card1 = new Card(Rank.TWO, Suit.DIAMONDS);
        System.out.println(card1);

        Card card2 = new Card(Rank.KING, Suit.CLUBS);
        System.out.println(card2);

        Rank[] ranks = Rank.values();
        for (Rank rank : ranks) {
            System.out.println(rank);
        }
        Serwis serwis = new Serwis(card1, card2);
        serwis.compare();

        Suit[] suits = Suit.values();
        for (Suit suit : suits) {
            for (Rank rank : ranks) {
                Card card = new Card(rank, suit);
                System.out.println(card);
            }
        }
    }
}
