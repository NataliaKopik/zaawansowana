package karty;

public class Serwis {
    Card card1;
    Card card2;

    public Serwis(Card card1, Card card2) {
        this.card1 = card1;
        this.card2 = card2;
    }

    public void compare(){
        if (card1.getStrength() > card2.getStrength()){
            System.out.println(card1 + " ma wyższą wartość (" +card1.getStrength() + ") niż " + card2 + "(" + card2.getStrength() + ").");
        }
        else if (card1.getStrength() < card2.getStrength()){
            System.out.println(card2 + " ma wyższą wartość (" +card2.getStrength() + ") niż " + card1 + "(" + card1.getStrength() + ").");        }
        else {
            System.out.println("Remis!");
        }
    }


}
