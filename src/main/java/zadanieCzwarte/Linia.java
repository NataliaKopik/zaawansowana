package zadanieCzwarte;

public class Linia {
    int dlugosc;
    char wypelnienie;

    public Linia(int dlugosc, char wypelnienie) {
        this.dlugosc = dlugosc;
        this.wypelnienie = wypelnienie;
    }

    public static void main(String[] args) {
        Linia linia1 = new Linia(5, '%');

        Linia linia2 = new Linia(6, '^');

        Linia linia3 = new Linia(1, '!');

        linia1.drukujLinie();
        linia2.drukujLinie();
        linia3.drukujLinie();

    }

    @Override
    public String toString() {
        return "Linia{" +
                "dlugosc=" + dlugosc +
                ", wypelnienie=" + wypelnienie +
                '}';
    }

    void drukujLinie(){
        System.out.println(String.valueOf(wypelnienie).repeat(dlugosc));
    }

}
