package zadanieCzwarte;

public class Samochod {

    String marka;
    int przebieg;
    int przebiegDoPrzegladu = 20_000;

    public Samochod(String marka, int przebieg) {
        this.marka = marka;
        this.przebieg = przebieg;
        this.przebiegDoPrzegladu = this.przebiegDoPrzegladu-przebieg;
    }

    void pokazMarkeIPrzebieg(){
        System.out.println("Marka: " + marka);
        System.out.println("Przebieg: " + przebieg);
    }

    @Override
    public String toString() {
        return "Marka: " + marka + "\n" + "Przebieg: " + przebieg + "\n" + "Przebieg do przeglądu: " + przebiegDoPrzegladu;
    }

    int zwiekszPrzebieg(int i){
        return przebieg += i;
    }

    int pokazIleKmDoPrzegladu(int i){
        return przebiegDoPrzegladu - i;
    }
}
