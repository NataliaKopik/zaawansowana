package interfejsy;

public class Daughter implements FamilyMember{
    private String name;

    public Daughter(String name) {
        this.name = name;
    }

    @Override
    public void przedstawSie() {
        System.out.println(name + ": i am daughter ;)");
    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
