package interfejsy;

public class Son implements FamilyMember{
    private String name;

    public Son(String name) {
        this.name = name;
    }

    @Override
    public void przedstawSie() {
        System.out.println(name + ": who’s asking?");
    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
