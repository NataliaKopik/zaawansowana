package interfejsy;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<FamilyMember> family = new ArrayList<>();
        family.add(new Father("Janusz"));
        family.add(new Mother("Grażyna"));
        family.add(new Daughter("Jesika"));
        family.add(new Son("Seba"));
        family.add(new FamilyMember() {
            @Override
            public boolean jestDorosły() {
                return false;
            }
        });

        for (FamilyMember person : family) {
            person.przedstawSie();
            person.jestDorosły();
        }
    }
}
