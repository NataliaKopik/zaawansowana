package interfejsy;

public class Mother implements FamilyMember{
    private String name;

    public Mother(String name) {
        this.name = name;
    }

    @Override
    public void przedstawSie() {
        System.out.println(name + ": i am mother");
    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
