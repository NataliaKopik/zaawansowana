package interfejsy;

public class Father implements FamilyMember{
    private String name;

    public Father(String name) {
        this.name = name;
    }

    @Override
    public void przedstawSie() {
        System.out.println(name + ": i am your father");
    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
