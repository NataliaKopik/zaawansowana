package stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Zd30 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            numbers.add(random.nextInt(100) + 1);
        }

        for (Integer number : numbers) {
            System.out.print(number + " ");
        }

        System.out.println("\na)");
        numbers.stream()
                .forEach(number -> System.out.print(number + " "));

        System.out.println("\nb)");
        numbers.stream()
                .sorted()
                .forEach(number -> System.out.print(number + " "));

        System.out.println("\nc)");
        System.out.print(numbers.stream()
                .filter(number -> number % 2 == 0)
                .count());

        System.out.println("\nd)");
        List<Integer> newList = numbers.stream()
                .filter(number -> number <= 50)
                .toList();
        newList.stream().forEach(number -> System.out.print(number + " "));

        System.out.println("\ne)");
        System.out.println(numbers.stream()
                .sorted(Comparator.reverseOrder())
                .findFirst()
                .get());    //Wypakowanie optionala
//lub
        System.out.println(numbers.stream()
                .sorted((num1, num2) -> num2.compareTo(num1)) //num1.compareTo(num2) zrobiłby kolejność wzrastającą
                        .findFirst()
                .get());
    }
}

