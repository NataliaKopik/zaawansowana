package Warsztat;

public class Paragon {
    private String rodzajUslugi = "wymiana opon";
    private int ileRazy;
    private int cena;

    public Paragon(int ileRazy) {
        this.ileRazy = ileRazy;
        this.cena = ileRazy * 300;
    }

    @Override
    public String toString() {
        return "Paragon{" +
                "rodzajUslugi='" + rodzajUslugi + '\'' +
                ", ileRazy=" + ileRazy +
                ", cena=" + cena +
                '}';
    }
}
