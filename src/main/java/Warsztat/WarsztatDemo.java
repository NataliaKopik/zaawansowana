package Warsztat;

import java.util.Random;

public class WarsztatDemo {
    public static void main(String[] args) {
        Kolo[] kola = new Kolo[4];
        for(int i = 0; i < kola.length; i++){
            kola[i] = new Kolo();
        }
        Kolo[] kola2 = new Kolo[4];
        for(int i = 0; i < kola2.length; i++){
            kola2[i] = new Kolo();
        }

        Samochod auto = new Samochod(kola);
        Samochod auto2 = new Samochod(kola2);

        System.out.println(auto + "\n" + auto2);

        auto.zlapGume();
        auto.zlapGume();

        System.out.println(auto + "\n" + auto2);

        Warsztat warsztat = new Warsztat(auto);
        warsztat.znajdzGume();
        warsztat = new Warsztat(auto2);
        warsztat.znajdzGume();

        System.out.println(auto + "\n" + auto2);
        System.out.println();
    }
}
