package Warsztat;

public class Warsztat {
    private Samochod autoWNaprawie;
    private int ileNapraw;
    private Paragon paragon;

    public Warsztat(Samochod auto){
        this.autoWNaprawie = auto;
    }

    public void znajdzGume(){
        Kolo[] kola = autoWNaprawie.getKola();
        for (Kolo kolo : kola){
            if (kolo.sprawdzCisnienie() < 2.2){
                napraw(kolo);
            }
        }
        if (ileNapraw != 0){
            paragon = stworzParagon();
        }
    }

    private void napraw(Kolo kolo) {
        kolo.wymienKolo(kolo.czyPrzebite(), kolo.sprawdzCisnienie());
        ileNapraw ++;
    }
    private Paragon stworzParagon(){
        Paragon paragon = new Paragon(ileNapraw);
        return paragon;
    }


}
