package Warsztat;

import java.util.Random;

public class Kolo {
    private double cisnienie = 2.2;
    private boolean przebite;

    public void zmniejszCisnienie(){
        przebite = true;
        cisnienie = 1.0;
    }

    public double sprawdzCisnienie() {
        return cisnienie;
    }

    public boolean czyPrzebite() {
        return przebite;
    }

    public void wymienKolo(boolean przebite, double cisnienie) {
        this.cisnienie = 2.2;
        this.przebite = false;

    }

    @Override
    public String toString() {
        return "Kolo{" +
                "cisnienie=" + cisnienie +
                ", przebite=" + przebite +
                '}';
    }
}
