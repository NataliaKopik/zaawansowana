package Warsztat;

import java.util.Arrays;
import java.util.Random;

public class Samochod {
    private Kolo[] kola;

    public Samochod(Kolo[] kola){
        this.kola = kola;
    }

    public void zlapGume(){
        Random random = new Random();
        int nrKola = random.nextInt(4);
        kola[nrKola].zmniejszCisnienie();
    }

    public Kolo[] getKola() {
        return kola;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "kola=" + Arrays.toString(kola) +
                '}';
    }
}
