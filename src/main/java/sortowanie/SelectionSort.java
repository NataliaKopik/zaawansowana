package sortowanie;
import java.util.LinkedList;
import java.util.List;

public class SelectionSort {

    public List sort(List<Integer> numbers) { //Sposób z przeniesieniem liczby wewnątrz listy
        Integer smallestNumber;
        int index = 0;
        for (int j = 0; j < numbers.size(); j++) {
            smallestNumber = numbers.get(j);
            for (int i = j; i < numbers.size(); i++) {
                if (numbers.get(i) <= smallestNumber) {
                    smallestNumber = numbers.get(i);
                    index = i;
                }
            }
            numbers.add(j, smallestNumber);
            numbers.remove(index + 1);
        }
        return numbers;
    }

    /*public List sort(List<Integer> numbers) { //Sposób z tworzeniem nowej listy i przenoszeniem tam liczby
        Integer smallestNumber;
        List<Integer> newList = new LinkedList<>();
        int size = numbers.size();
        for (int j = 0; j < size; j++) {
            smallestNumber = numbers.get(0);
            for (int i = 1; i < numbers.size(); i++) {
                if (numbers.get(i) < smallestNumber) {
                    smallestNumber = numbers.get(i);
                }
            }
            newList.add(smallestNumber);
            numbers.remove(smallestNumber);
        }
        return newList;
    }*/
}

class SortingDemo {

    public static void main(String[] args) {
        SelectionSort selectionSort = new SelectionSort();
        List<Integer> numbers = new LinkedList<>(List.of(12, 34, 9, 55, 44, 1, 7, 99, 2, 44)); //10 liczb
        System.out.println(numbers);
        System.out.println(selectionSort.sort(numbers));
    }
}