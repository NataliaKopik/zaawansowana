package sortowanie;

import java.util.LinkedList;
import java.util.List;

public class InsertionSort {
    public List sort(List<Integer> numbers) {
        for (int i = 0; i < numbers.size() - 1; i++) {
            Integer badany = numbers.get(i + 1);
            Integer porownywany = numbers.get(i);
            if (badany >= porownywany) {
                continue;
            }
            while (badany < porownywany && numbers.indexOf(porownywany) > 0) {
                porownywany = numbers.get(numbers.indexOf(porownywany) - 1);
            }
            if (badany > porownywany) {
                porownywany = numbers.get(numbers.indexOf(porownywany) + 1);
            }
            numbers.remove(i + 1);
            System.out.println(numbers);
            numbers.add(numbers.indexOf(porownywany), badany);
            System.out.println(numbers);
        }
        return numbers;
    }

}

class Demo {

    public static void main(String[] args) {
        InsertionSort insertionSort = new InsertionSort();
        List<Integer> numbers = new LinkedList<>(List.of(12, 34, 9, 55, 44, 1, 7, 99, 2, 44)); //10 liczb
        System.out.println(numbers);
        System.out.println(insertionSort.sort(numbers));
    }
}
