package dziedziczenie;

import java.util.ArrayList;
import java.util.List;

public class TownDemo {
    public static void main(String[] args) {
        Townsman Jack = new Townsman("Jack");
        Soldier John = new Soldier("John");
        Peasant Bob = new Peasant("Bob");
        King Edward = new King("Edward");

        List<Citizen> citizens = new ArrayList<>();
        citizens.add(Jack);
        citizens.add(John);
        citizens.add(Bob);
        citizens.add(Edward);

        Town town = new Town(citizens);
        System.out.println(town);
        System.out.println(town.howManyCanVote());
        town.whoCanVote();
    }
}
