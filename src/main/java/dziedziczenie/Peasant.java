package dziedziczenie;

public class Peasant extends Citizen{
    @Override
    boolean canVote() {
        return false;
    }

    public Peasant(String name) {
        super(name);
    }
}
