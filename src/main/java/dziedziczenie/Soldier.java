package dziedziczenie;

public class Soldier extends Citizen{
    @Override
    boolean canVote() {
        return true;
    }

    public Soldier(String name) {
        super(name);
    }
}
