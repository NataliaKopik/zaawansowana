package dziedziczenie;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

public class Town {

    List<Citizen> citizens = new ArrayList<>();

    public Town(List<Citizen> citizens) {
        this.citizens = citizens;
    }

    @Override
    public String toString() {
        return "Town{" +
                "citizens=" + citizens +
                '}';
    }

    public int howManyCanVote(){
        int counter = 0;
        for (Citizen citizen : citizens){
            if(citizen.canVote()){
                counter++;
            }
        }
        return counter;
    }

    public void whoCanVote(){
        for (Citizen citizen : citizens){
            if(citizen.canVote()){
                System.out.println(citizen.getName());
            }
        }
    }
}
