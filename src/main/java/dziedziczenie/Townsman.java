package dziedziczenie;

public class Townsman extends Citizen{
    @Override
    boolean canVote() {
        return true;
    }

    public Townsman(String name) {
        super(name);
    }
}
