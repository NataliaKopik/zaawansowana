package zadanieTrzecie;

public class Samochod {

    String marka;
    int przebieg;
    int przebiegDoPrzegladu = 20_000;

    public Samochod(String marka, int przebieg) {
        this.marka = marka;
        this.przebieg = przebieg;
        this.przebiegDoPrzegladu = this.przebiegDoPrzegladu-przebieg;
    }

    void pokazMarkeIPrzebieg(){
        System.out.println("Marka: " + marka);
        System.out.println("Przebieg: " + przebieg);
    }

    int zwiekszPrzebieg(int i){
        return przebieg += i;
    }

    int pokazIleKmDoPrzegladu(int i){
        return przebiegDoPrzegladu - i;
    }
}
