package zadanieTrzecie;

public class SamochodDemo {
    public static void main(String[] args) {
        Samochod auto1 = new Samochod("BMW", 5_000);
        Samochod auto2 = new Samochod("Kia", 19_000);

        auto1.pokazMarkeIPrzebieg();
        auto2.pokazMarkeIPrzebieg();

        int trasa = 1000;
        System.out.println(auto1.zwiekszPrzebieg(trasa));
        System.out.println(auto2.zwiekszPrzebieg(trasa));

        System.out.println(auto1.pokazIleKmDoPrzegladu(trasa));
        System.out.println(auto2.pokazIleKmDoPrzegladu(trasa));

        System.out.println(auto1.toString());
    }
}
