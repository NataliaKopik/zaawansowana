package Wyjatek;

import java.util.Scanner;

public class Input {

    public int readNumber() {
        int number=0;
        boolean isError;
        do {
            try {
                Scanner scanner = new Scanner(System.in);
                isError = false;
                System.out.println("Wpisz liczbę:");
                number = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("Wpisz tylko liczby!");
                isError = true;
                e.printStackTrace();
            }
        } while (isError);
        return number;
    }


    public static void main(String[] args) {
        Input input = new Input();
        int number = input.readNumber();
        System.out.println("Podano " + number);
    }


}

