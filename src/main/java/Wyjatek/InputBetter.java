package Wyjatek;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputBetter {

    public int readNumber() {
        int number;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz liczbę:");
        try {
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Wpisz tylko liczby!");
            return readNumber();
        }
        return number;
    }

    public static void main(String[] args) {
        InputBetter input = new InputBetter();
        int number = input.readNumber();
        System.out.println("Podano " + number);
    }
}

