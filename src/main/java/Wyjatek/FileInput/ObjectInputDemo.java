package Wyjatek.FileInput;

import java.util.ArrayList;
import java.util.List;

public class ObjectInputDemo {
    public static void main(String[] args) {
        FileInput input = new FileInput();
        List<Book> books = new ArrayList<>();
        try {
            books = input.readBooks();
            System.out.println(books);
        } catch (BookMappingException e) {
            System.out.println("Wystąpił wyjątek:");
            System.out.println(e.getMessage());
            System.out.println(e.getLine());
        }

        FileConnection fileConnection = new FileConnection();
        try {
            fileConnection.connect();
        } catch (FileConnectionException e) {
            System.out.println("Błąd połączenia!");
        } finally {
            System.out.println(fileConnection);
        }



        ObjectInput objectInput = new ObjectInput();
        try {
            String s = objectInput.getDataFromObject((books));
            System.out.println(s);
        } catch (FileDbConnectionException a) {
            System.out.println("Bład! Plik tekstowy jest pusty!");
        } finally {
            fileConnection.disconnect();
            System.out.println(fileConnection);
        }
    }
}
