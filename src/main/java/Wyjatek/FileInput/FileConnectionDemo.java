package Wyjatek.FileInput;

public class FileConnectionDemo {
    public static void main(String[] args) {
        FileConnection fileConnection = new FileConnection();
        try {
            fileConnection.connect();
        } catch (FileConnectionException e) {
            System.out.println("Błąd połączenia!");
        }
        System.out.println(fileConnection);
    }
}
