package Wyjatek.FileInput;

import java.util.Random;

public class FileConnection{
    private boolean isConnceted = false;

    public void connect() throws FileConnectionException {
        Random random = new Random();
        int r = random.nextInt(4);
        if (r == 0){
            throw new FileConnectionException();
        }
        isConnceted = true;
    }

    public void disconnect(){
        isConnceted = false;
    }

    @Override
    public String toString() {
        return "FileConnection{" +
                "isConnceted=" + isConnceted +
                '}';
    }
}
