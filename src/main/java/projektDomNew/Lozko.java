package projektDomNew;

public class Lozko {
    private int licznik;

    public Lozko(int i){
        this.licznik = i;
    }

    public void zmienPosciel(){
        licznik = 0;
    }

    @Override
    public String toString() {
        return "Lozko{" +
                "licznik=" + licznik +
                '}';
    }
}
