package projektDomNew;

public class DomDemo {

    public static void main(String[] args) {
        Okno[] okna = new Okno[4];

        for (int i = 0; i < okna.length; i++) {
            okna[i] = new Okno();
        }

        for(Okno okno : okna){
            okno.otworzOkno();
        }

        Pokoj pokoj1 = new Pokoj();
        Pokoj pokoj2 = new Pokoj(new Lozko(5), okna);
        Pokoj[] pokoje = {pokoj1, pokoj2};
        Dom dom1 = new Dom(pokoje);
        System.out.println(dom1);
    }

}
