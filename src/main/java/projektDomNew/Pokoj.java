package projektDomNew;

import java.util.Arrays;

public class Pokoj {
    Lozko lozko;
    Okno[] okna;

    public Pokoj (Lozko lozko, Okno[] okna){
        this.lozko = lozko;
        this.okna = okna;
    }

    public Pokoj() {
        this.lozko = new Lozko(5);
        this.okna = new Okno[2];
        for (int i = 0; i < this.okna.length; i++) {
            this.okna[i] = new Okno();
        }
    }

    public static void main(String[] args) {
        Pokoj pokojProba = new Pokoj();
        System.out.println(pokojProba);
        pokojProba.posprzataj(pokojProba);
        System.out.println(pokojProba);

    }

    void posprzataj(Pokoj pokoj){
        pokoj.lozko.zmienPosciel();
        for (Okno okno : okna){
            okno.otworzOkno();
        }
    }

    @Override
    public String toString() {
        return "Pokoj{" +
                "lozko=" + lozko +
                ", okna=" + Arrays.toString(okna) +
                '}';
    }

}

