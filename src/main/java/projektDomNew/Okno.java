package projektDomNew;

public class Okno {
    private boolean stan;

    public void otworzOkno(){
        stan = true;
    }

    @Override
    public String toString() {
        return "Okno{" +
                "stan=" + stan +
                '}';
    }
}
