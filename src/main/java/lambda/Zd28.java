package lambda;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Zd28 {
    public static void main(String[] args) {
        Function<String, Integer> textLenght = text -> text.length();
        System.out.println(textLenght.apply("natalia"));
        Predicate<String> has5char = text -> text.length() >= 5;
        System.out.println(has5char.test("nat"));
        Supplier<Double> randomDouble = () -> {
            Random random = new Random();
            return (random.nextDouble(0.4) + 0.1);
        };
        System.out.format("%.1f\n", randomDouble.get());
        Function<String, String> upperCaseText = text -> text.toUpperCase();
        System.out.println(upperCaseText.apply("natalia"));
    }
}


/*
Stosując gotowe klasy interfejsów funkcyjnych (Function, Consumer, Supplier, Comparator, Predicate) zaimplementuj i przetestuj 5 funkcji:
funkcja przyjmująca tekst i zwracająca jego długość
funkcja sprawdzająca czy przekazany tekst ma minimum 5 znaków
funkcja dostarczająca losową liczbę double z przedziału 0,1-0,5
funkcja drukująca tekst wielkimi literami

 */