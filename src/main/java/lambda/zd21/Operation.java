package lambda.zd21;

public interface Operation {
    int method (int first, int second);
}
