package lambda.zd21;

public class DemoZd21 {
    public static void main(String[] args) {
        Operation addition = new Operation() {
            @Override
            public int method(int first, int second) {
                return first + second;
            }
        };

        System.out.println(addition.method(2, 5));

        Operation subtraction = (first, second) -> first - second; // lambda

        System.out.println(subtraction.method(2,5));
    }

}
