package lambda;

import java.util.Comparator;
import java.util.Random;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Zd23 {
    public static void main(String[] args) {
        Supplier<Integer> randomValue = () -> {
            Random random = new Random();
            return random.nextInt(10) + 1;
        };
        System.out.println(randomValue.get());

        Predicate<String> isPasswordLongEnought = password -> {
            return password.length() >= 5;
        };
        System.out.println(isPasswordLongEnought.test("random"));

        Comparator<String> compareLength = (text1, text2) -> {
            if (text1.length() == text2.length())
                return 0;
            else if (text1.length() > text2.length())
                return 1;
            else
                return -1;
        };
        System.out.println(compareLength.compare("tekst dłuższy", "tekst"));
        // lub
        Comparator<String> compareLength2 = (text1, text2) -> {
            return text1.length() - text2.length();
        };
        if (compareLength2.compare("tekst dłuższy", "tekst") > 0) {
            System.out.println("Pierwszy tekst jest dłuższy");
        } else if (compareLength2.compare("tekst dłuższy", "tekst") < 0) {
            System.out.println("Drugi tekst jest dłuższy");
        } else {
            System.out.println("Oba są równe");
        }
    }
}