package lambda.zd22;

import java.lang.reflect.Array;

public class DemoZd22 {
    public static void main(String[] args) {

        Printer scream = text -> text.toUpperCase() + "!";

        Printer wordFromFirstLetters = text -> {
            String[] words = text.split(" ");
            String result = "";
            for (int i = 0; i < words.length; i++) {
                result += words[i].charAt(0);
            }
            return result;
        };

        System.out.println(scream.print("Hello"));
        System.out.println(wordFromFirstLetters.print("Hello plus same other words"));

    }
}
