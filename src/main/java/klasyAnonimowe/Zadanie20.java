package klasyAnonimowe;

public class Zadanie20 {
    public static void main(String[] args) {

        Equation equation = new Equation() {
            @Override
            public int execute(int number1, int number2) {
                return number1 + number2;
            }
        };
        System.out.print(equation.execute(2, 5));
    }
}

interface Equation {
    int execute(int number1, int number2);
}