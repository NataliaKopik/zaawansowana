package klasyAnonimowe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Zadanie19 {
    public static void main(String[] args) {
        List<Tree> forest = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Tree drzewo = new Tree();
            forest.add(drzewo);
        }

        Tree magiczneDrzewo = new Tree() {
            @Override
            void display() {
                System.out.println("magiczne drzewo");
            }
        };

        forest.add(magiczneDrzewo);

        Collections.shuffle(forest);

        for (Tree drzewo : forest) {
            drzewo.display();
        }
    }
}


class Tree {
    void display() {
        System.out.println("zwykłe drzewo");
    }
}