package zadaniePierwsze;

public class Samochod {

    String marka;
    int przebieg;
    int przebiegDoPrzegladu = 20_000;

    void pokazMarkeIPrzebieg(){
        System.out.println("Marka: " + marka);
        System.out.println("Przebieg: " + przebieg);
    }

    int zwiekszPrzebieg(int i){
        return przebieg += i;
    }

    int pokazIleKmDoPrzegladu(int i){
        return przebiegDoPrzegladu - i;
    }
}
