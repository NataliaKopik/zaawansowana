package zadaniePierwsze;

public class SamochodDemo {
    public static void main(String[] args) {
        Samochod auto1 = new Samochod();
        Samochod auto2 = new Samochod();

        auto1.marka = "BMW";
        auto1.przebieg = 5_000;
        auto1.przebiegDoPrzegladu -= auto1.przebieg;

        auto2.marka = "Kia";
        auto2.przebieg = 19_000;
        auto2.przebiegDoPrzegladu -= auto2.przebieg;

        auto1.pokazMarkeIPrzebieg();
        auto2.pokazMarkeIPrzebieg();

        int trasa = 1000;
        auto1.zwiekszPrzebieg(trasa);
        auto2.zwiekszPrzebieg(trasa);

        auto1.pokazIleKmDoPrzegladu(trasa);
        auto2.pokazIleKmDoPrzegladu(trasa);
    }
}
