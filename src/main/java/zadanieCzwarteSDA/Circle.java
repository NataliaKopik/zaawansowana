package zadanieCzwarteSDA;

public class Circle extends Shape {
    private int  radius;

    public Circle() {
        this.radius = 1;
    }

    public Circle(String color, boolean isFilled, int radius) {
        super(color, isFilled);
        this.radius = radius;
    }

    public double getArea(){
        return radius * radius * Math.PI;
    }

    public double getPerimeter(){
        return radius * 2 * Math.PI;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return String.format("Circle with radius=%d which is a subclass off %s", radius, super.toString());
    }
}
