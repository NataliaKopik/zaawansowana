package zadanieCzwarteSDA;

public class Rectangle extends Shape {
    protected double width;
    protected double length;

    public Rectangle() {
        this.width = 1;
        this.length = 1;
    }

    public Rectangle(String color, boolean isFilled, double width, double length) {
        super(color, isFilled);

        this.width = width;
        this.length = length;
    }

    public double getPerimeter(){
        return (width * 2) + (length *2);
    }

    public double getArea(){
        return width * length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return String.format("Rectangle with width=%.2f and length=%.2f which is a subclass of %s", width, length, super.toString());
    }
}
