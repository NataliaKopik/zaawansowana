package zadanieCzwarteSDA;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = {
                new Circle("blue", false, 6),
                new Rectangle("green", false, 2, 7),
                new Square("pink", true, 5)};

        for (Shape shape : shapes) {
            System.out.println(shape);
            System.out.println(shape.getArea());
            System.out.println(shape.getPerimeter());
        }
    }
}
