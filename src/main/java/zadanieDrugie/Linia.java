package zadanieDrugie;

public class Linia {
    int dlugosc;
    char wypelnienie;

    public static void main(String[] args) {
        Linia linia1 = new Linia();
        linia1.dlugosc = 5;
        linia1.wypelnienie = '%';

        Linia linia2 = new Linia();
        linia2.dlugosc = 6;
        linia2.wypelnienie = '^';

        Linia linia3 = new Linia();
        linia3.dlugosc = 1;
        linia3.wypelnienie = '!';

        linia1.drukujLinie();
        linia2.drukujLinie();
        linia3.drukujLinie();

    }

    void drukujLinie(){
        System.out.println(String.valueOf(wypelnienie).repeat(dlugosc));
    }

}
