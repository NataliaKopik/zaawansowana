package zadaniePiate;

public class Linia {
    private int dlugosc;
    private char wypelnienie;

    public Linia(int dlugosc, char wypelnienie) {
        this.dlugosc = dlugosc;
        this.wypelnienie = wypelnienie;
    }

    public static void main(String[] args) {
        Linia linia1 = new Linia(5, '%');

        Linia linia2 = new Linia(6, '^');

        Linia linia3 = new Linia(1, '!');

        linia1.drukujLinie();
        linia2.drukujLinie();
        linia3.drukujLinie();

    }

    void drukujLinie(){
        System.out.println(String.valueOf(wypelnienie).repeat(dlugosc));
    }

    @Override
    public String toString() {
        return "Linia{" +
                "dlugosc=" + dlugosc +
                ", wypelnienie=" + wypelnienie +
                '}';
    }

    public int getDlugosc(){
        return dlugosc;
    }

    public void setDlugosc(int dlugosc) {
        this.dlugosc = dlugosc;
    }
}
