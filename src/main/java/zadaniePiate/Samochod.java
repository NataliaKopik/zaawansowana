package zadaniePiate;

public class Samochod {

    private String marka;
    private int przebieg;
    private int przebiegDoPrzegladu = 20_000;

    public Samochod(String marka, int przebieg) {
        this.marka = marka;
        this.przebieg = przebieg;
        this.przebiegDoPrzegladu = this.przebiegDoPrzegladu-przebieg;
    }

    void pokazMarkeIPrzebieg(){
        System.out.println("Marka: " + marka);
        System.out.println("Przebieg: " + przebieg);
    }

    int zwiekszPrzebieg(int i){
        return przebieg += i;
    }

    int pokazIleKmDoPrzegladu(int i){
        return przebiegDoPrzegladu - i;
    }

    @Override
    public String toString() {
        return "Marka: " + marka + "\n" + "Przebieg: " + przebieg + "\n" + "Przebieg do przeglądu: " + przebiegDoPrzegladu;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }
}
